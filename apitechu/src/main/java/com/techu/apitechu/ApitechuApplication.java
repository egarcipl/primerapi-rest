package com.techu.apitechu;

import com.techu.apitechu.models.ProductModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
//import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
//@RestController
public class ApitechuApplication {

	//Propiedad de la aplicación: listado de product model
    // si está en cursiva indica que es estático

	public static ArrayList<ProductModel> productModels;

	public static void main(String[] args) {

		SpringApplication.run(ApitechuApplication.class, args);

		ApitechuApplication.productModels = ApitechuApplication.getTestData();

	}

	private static ArrayList<ProductModel> getTestData(){
		ArrayList<ProductModel> productModels = new ArrayList<>();

		productModels.add(new ProductModel(
				"1"
				,"Producto 1"
				,10
		));

		// La f indica que es float, si no la ponemos se piensa que es un double
		productModels.add(new ProductModel(
				"2"
				,"Producto 2"
				,20.2f
		));

		productModels.add(new ProductModel(
				"3"
				,"Producto 3"
				,30.3f
		));

		productModels.add(new ProductModel(
				"4"
				,"Producto 4"
				,40
		));

		return productModels;
	}


//	@RequestMapping("/hello")
//	public String greetings(){
//			return "Hola Mundo desde API Tech U!";
//	}

}
