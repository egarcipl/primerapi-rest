package com.techu.apitechu.controllers;

import com.techu.apitechu.ApitechuApplication;
import com.techu.apitechu.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductController {

    static final String APIBaseURL = "/apitechu/v1";

    //ProductModel son nuestros productos, el término MVC que representa las entidades
    @GetMapping(APIBaseURL + "/products")
    public ArrayList<ProductModel> getProducts(){
        System.out.println("getProducts");

        return ApitechuApplication.productModels;

    }

    // Url con un parámetro
    @GetMapping(APIBaseURL + "/products/{id}")

    /*
    Con PathVariable recuperamos el parametro id que nos llega
    Todo esto que tiene la arroba son utilidades del framework
    */

    public ProductModel getProductById(@PathVariable String id){
        System.out.println("getProductById");
        System.out.println("id es " + id);

        ProductModel result = new ProductModel();

        for(ProductModel product : ApitechuApplication.productModels){
            if(product.getId().equals(id)){
                result = product;
            }
        }

        return result;

    }

    //Peticion para crear el objeto, aqui como tienen que llegar datos hay que incorporar un RequestBody,
    //esto es una utilidad que nos da el framework, pues en otro caso lo que recibiríamos en el body
    //sería una línea que iríamos por código parseando a cada variable. De esta forma, el framework
    //convierte lo que enviamos directamente desde postman a un objeto identificando el tipo de objeto
    //y su constructor
    //Nota: no hay que poner /createproducts pues el create ya está implicito en el Post
    @PostMapping(APIBaseURL + "/products")
    public ProductModel createProduct(@RequestBody ProductModel newProduct){

        System.out.println("createProduct");
        System.out.println("La id del nuevo producto es " + newProduct.getId());
        System.out.println("La descripcion del nuevo producto es " + newProduct.getDesc());
        System.out.println("El precio del nuevo producto es " + newProduct.getPrice());

        ApitechuApplication.productModels.add(newProduct);

        return newProduct;
    }

    @PutMapping(APIBaseURL + "/products/{id}")
    public ProductModel updateProduct(@RequestBody ProductModel product, @PathVariable String id){

        System.out.println("updateProduct");
        System.out.println("La id del producto a actualizar en parametro en URL es " + id);
        System.out.println("La id del producto a actualizar es " + product.getId());
        System.out.println("La descripcion del producto a actualizar es " + product.getDesc());
        System.out.println("El precio del producto a actualizar es " + product.getPrice());

        for(ProductModel productInList : ApitechuApplication.productModels) {
            if (productInList.getId().equals(id)){
                productInList.setId(product.getId());
                productInList.setDesc(product.getDesc());
                productInList.setPrice(product.getPrice());
            }
        }

        return product;

    }

    @DeleteMapping(APIBaseURL + "/products/{id}")
    public ProductModel deleteProduct(@PathVariable String id){

        System.out.println("deleteProduct");
        System.out.println("La id del producto a borrar en parametro en URL es " + id);

        ProductModel result = new ProductModel();
        boolean foundProduct = false;

        for (ProductModel product : ApitechuApplication.productModels){
            if (product.getId().equals(id)){
                System.out.println("Producto para borrar encontrado");
                foundProduct = true;
                result = product;
            }
        }

        if(foundProduct==true)
        {
            System.out.println("Borrando producto");
            ApitechuApplication.productModels.remove(result);
        }

        return new ProductModel();

       }
/*
    @PatchMapping(APIBaseURL + "/products/{id}")
    public ProductModel patchProduct(@RequestBody ProductModel product, @PathVariable String id) {

        System.out.println("patchProduct");
        System.out.println("La id del producto a actualizar en parametro en URL es " + id);
        System.out.println("La descripcion del producto a actualizar es " + product.getDesc());
        System.out.println("El precio del producto a actualizar es " + product.getPrice());

        for (ProductModel productInList : ApitechuApplication.productModels) {

            if (productInList.getId().equals(id)) {

                if (product.getDesc() != "" && product.getDesc() != null ) {
                    System.out.println("Descripcion ANTES de actualizar es " + productInList.getDesc());
                    productInList.setDesc(product.getDesc());
                    System.out.println("Descripcion DESPUES de actualizar es " + productInList.getDesc());
                }
                if (product.getPrice() != 0) {
                    System.out.println("Precio ANTES de actualizar es " + productInList.getPrice());
                    productInList.setPrice(product.getPrice());
                    System.out.println("Precio DESPUES de actualizar es " + productInList.getPrice());
                }
            }
        }
        return product;

    }
    */

    @PatchMapping(APIBaseURL + "/products/{id}")
    public ProductModel patchProfe(@RequestBody ProductModel product, @PathVariable String id) {

        System.out.println("patchProduct");
        System.out.println("La id del producto a actualizar en parametro en URL es " + id);
        System.out.println("La descripcion del producto a actualizar es " + product.getDesc());
        System.out.println("El precio del producto a actualizar es " + product.getPrice());

        ProductModel result = new ProductModel();

        for (ProductModel productInList : ApitechuApplication.productModels) {

            if (productInList.getId().equals(id)) {

                result=productInList;
                if (product.getDesc() != null ) {
                    System.out.println("Descripcion ANTES de actualizar es " + productInList.getDesc());
                    productInList.setDesc(product.getDesc());
                    System.out.println("Descripcion DESPUES de actualizar es " + productInList.getDesc());
                }
                if (product.getPrice() > 0) {
                    System.out.println("Precio ANTES de actualizar es " + productInList.getPrice());
                    productInList.setPrice(product.getPrice());
                    System.out.println("Precio DESPUES de actualizar es " + productInList.getPrice());
                }
            }
        }
        return result ;

    }



}
