package com.techu.apitechu.models;

public class ProductModel {
    private String id;
    private String desc;
    private float price;

    //Constructores de la clase


    public ProductModel() {
    }

    public ProductModel(String id, String desc, float price) {
        this.id = id;
        this.desc = desc;
        this.price = price;
    }

    public String getId() {
        return this.id;
    }

    public String getDesc() {
        return this.desc;
    }

    public float getPrice() {
        return this.price;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
